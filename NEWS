Version 2.9 --

  * Build and self-test improvements.

Version 2.8 -- Sun, 17 Jan 2021 18:13:43 -0500

  * Resolved Ubuntu bug 1151766
  * The -X parser is now the default
  * tests moved to ./tests
  * repairs to pdiff(1) -- anybody ever use this?

Version 2.7b -- Thu, 14 Jan 2021 14:59:07 +0100

  * Moved web site to https://gitlab.com/jas/pmccabe/
  * Fix typos in pmccabe.1.
  * The 'description' file was renamed to README.
  * The ChangeLog file renamed to NEWS and contains more history.
  * Non-source files were removed, notably pmccabe.html that
    can be re-generated from pmccabe.1.
  * Don't infloop on architectures with unsigned char (regression
    introduced in 2.7).
  * Source code re-indented using 'make indent' (only minor changes
    for newly added code).
  * The -x parser is still the default, switch to -X is planned for 2.8.

Version 2.7 -- Fri, 20 May 2011 17:37:45 -0400

  * moved web site to http://people.debian.org/~bame/pmccabe
  * Fix bugs from Ahmad Jbara <ahmadjbara@gmail.com>
    1. no longer counts ^ as part of complexity like &&
    2. statement count now includes # of switch (but not case) statements
       which will break people's regression tests WARNING! WARNING!
  * Inspired by and/or code donated by Matt Hargett:
  * (LP: #499591) "add threshold capability" from Matt Hargett
  * (LP: #659550) -
  * added -x/-X options to select the old or Matt's "cpp" algorithm
  * WARNING WARNING WARNING!!!! in version 2.8, -X will become the
    default and which will likely break people's existing regression tests
  * used indent(1) to format the source -st -bap -bad -d0 -bli0 -nce
    -cli0 -ss -npcs -bs -di1 -nbc -psl -i4 -lp -ip0 -nfca -nfc1
  * made some things boolean and fixed a bunch of compiler warnings
  * added -fwhole-program speedup

Version 2.6 -- Fri, 06 Aug 2010 14:18:25 -0400

  * (LP: #364338) pdiff script change has bug (ubuntu)
  * (LP: #499589) infinite loop with namespaced struct variable declaration
  * Thanks to matt_hargett for bugs and patches
  * Fixed a problem parsing DOS-format CRLF files

Version 2.5 -- Wed, 30 Jul 2008 15:21:28 -0600

  * updated codechanges and pdiff to be bashism safe
  * updated test files

Version 2.4 -- Mon, 13 Aug 2007 21:14:22 -0600

  * add support for C++ namespaces

Version 2.3 -- Wed, 26 Nov 2003 12:06:55 -0700

  * you can use "class" as an identifier in C code now.  Thanks to
    xianjie zhang for an excellent bug report.

12 Mar 2003

    pmccabe 2.2-3 Debian package (for sarge/unstable) uploaded. Red
    Hat version is pmccabe-2.2-4.i386.rpm, created with alien(1).

Version 2.2 -- Tue, 17 Dec 2002 10:07:41 -0700
    Prepare for releasing to Open Source, includes Debianizing.

Version 2.1
    Allow non-K&R 'const' in K&R-style function definitions (yuck).

Version 2.0
    Parse destructor names properly for functions implemented
    within their class definitions.

    Support declarations of classes within functions and functions
    within classes, recursively.

    Add -f and -F options to enable per-file statistics printing.

    Add -V option to get the current version number.

    Add -n option to calculate non-commented source lines directly.
    Output format compatible with 'anac'.

    Count C-preprocessor lines properly with -d and -n options,
    previously was not counting them as non-commented lines.

    Keep track of # statements in a file which are outside the scope
    of functions (e.g., variable declarations).

    Parsing 'struct foo function() {return;}' and some related
    constructs used to confuse the parser -- fixed.

    Remove a bunch of dead code.

    More consistent error messages.

12 Feb 2003
    First external web pages prototype

Jan 2002
    Announced intent to package for Debian.

Dec 2002
    Hewlett-Packard agrees to release pmccabe

July 1998
    Port to win32

May 1993
    Get rid of Unixy multi-process architecture so it'll work on DOS.

July 1992
    pmccabe begins as a personal project within HP.
